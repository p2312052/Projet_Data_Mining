# Remise du projet de Data mining 
Membres du groupe :
    - AZIZI Anis
    - BOUDJEBBOUR Maya 
    - RABEHI Amira sabrine

lien vers le colab en Lecture : 
    https://colab.research.google.com/drive/1MivB5auvZ6mor8I6HVyRyI7cMEXTfbeB?usp=sharing

---

### **Répartition des Tâches**

Dans le cadre de ce projet, chaque membre de l'équipe a contribué à des étapes spécifiques et complémentaires pour assurer une analyse approfondie des données et des résultats. Voici la répartition des tâches :

- **Maya :**  
  - Analyse de la corrélation entre les variables.  
  - Détection des outliers à l'aide de l'algorithme Isolation Forest.  
  - Application et analyse des méthodes de clustering (PCA, K-Means, DBSCAN).  
  - Extraction de motifs fréquents avec l'algorithme Apriori.

- **Anis :**  
  - Exploration et visualisation initiale des données.  
  - Nettoyage et pré-traitement des données (gestion des valeurs manquantes, encodage, etc.).  
  - Application et amélioration des techniques de clustering, notamment avec T-SNE et SOM.  
  - Développement des modèles de prédiction (régressions et algorithmes avancés).

- **Amira :**  
  - Mise en place du système de recommandation en répondant à trois questions clés :  
    1. Identification des modèles populaires par région.  
    2. Recommandation selon le budget des acheteurs.  
    3. Recommandation croisée par type et budget.  
  - Amélioration et optimisation des résultats du clustering K-Means.

